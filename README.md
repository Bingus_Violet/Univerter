# Univerter

A web downloader & converter for videos with no client-sided javascript. It can be found at https://univerter.dev

Currently supports a wide variety of formats and sites, but if you have any other formats you want supported, please open an issue.
Formats added to Univerter must be supported by FFmpeg
Support for sites will only be added if those sites are supported by yt-dlp.

# Installation

There are 2 main ways to install Univerter:

## Docker

The docker image can be found at https://hub.docker.com/r/bingusviolet/univerter
Port 8080 (or whatever you set the environment variable to) is exposed inside the container. It can be exposed with `-p`
Example:
```bash
docker run -p 8080:8080 bingusviolet/univerter
```
This will allow it to be found on port 8080 (E.G. `localhost:8080`)

## Node

1. Clone the repository

```bash
git clone https://github.com/Violets-puragtory/YoutubeConverter
cd YoutubeConverter
```

2. Install Dependancies

- [ffmpeg](https://github.com/FFmpeg/FFmpeg)
- [node](https://github.com/nodejs/node)
- [npm](https://github.com/npm/cli)
- [yt-dlp](https://github.com/yt-dlp/yt-dlp)

3. Download NodeJS Dependencies

```bash
npm install
```

## Starting it

```bash
npm start
```

# Environment Variables

`PORT`= [Preffered Port] (8080 if unspecified)
`MAX_FILESIZE` = [Max file size in MB] (Only applies to 1080p>, don't rely on this feature yet though, as it is unfinished.)

# To-Do
- [ ] Save settings (per session)
- [ ] Proper config
- [ ] Advanced options menu
- [ ] Add option for subtitles to file
- [ ] Video Searching
- [ ] Video Caching Options
- [ ] Properly cleanup unnessacary files
- [ ] Option to disable 1080p+
- [ ] Option to disable Conversion

# Instances
Here is a list of all currently public instances. If you want to add your instance to this list, please make an issue or pull request!

- https://univerter.dev (official)
- https://yt.violets-purgatory.dev (official)