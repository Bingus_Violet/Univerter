FROM node:20

RUN apt-get update \
    && apt install python3 pip ffmpeg -y \
    && python3 -m pip install -U yt-dlp --break-system-packages

WORKDIR /usr/src/app

COPY package*.json ./
RUN npm install

COPY . .

EXPOSE 8080

CMD [ "node", "index.js" ]